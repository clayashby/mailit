class UserMailer < ActionMailer::Base
   default :from => "kaimana@gmail.com"

  def registration_confirmation(user)
  	@user = user
  	attachments["rails.png"] = File.read("#{Rails.root}/public/rails.png")
    mail(:to => "#{user.name} <#{user.email}>", :subject => "Grandpa has taken AM medication")
  end
end
