class Event < ActiveRecord::Base
  attr_accessible :comment, :desc, :scheduled, :title
end
