class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.string :desc
      t.datetime :scheduled
      t.string :comment

      t.timestamps
    end
  end
end
