class CreateScenes < ActiveRecord::Migration
  def change
    create_table :scenes do |t|
      t.string :title
      t.string :actor1
      t.text :body

      t.timestamps
    end
  end
end
